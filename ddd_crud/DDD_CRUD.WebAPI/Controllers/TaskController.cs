using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DDD_CRUD.Domain.Entities;
using DDD_CRUD.Domain.ObjectValue;

namespace DDD_CRUD.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Task")]
    public class TaskController : Controller
    {

        private IList<Tarefa> _tarefas;

        public TaskController()
        {
            _tarefas = new List<Tarefa>()
            {
                new Tarefa(){ Id = 1, Nome = "Tarefa 1", Concluida = false},
                new Tarefa(){ Id = 2, Nome = "Tarefa 2", Concluida = true}
            };
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return new OkObjectResult(_tarefas);
            }
            catch (Exception ex)
            {
                return BadRequest(error: ex.ToString());
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var tarefa = _tarefas.Where(x => x.Id == id);

                if (!tarefa.Any())
                    return NotFound();

                return new OkObjectResult(tarefa);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody]Tarefa t)
        {
            try {
                _tarefas.Add(t);
                return new OkObjectResult(_tarefas);
            }
            catch (Exception ex)
            {
                return BadRequest(error: ex.ToString());
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]Tarefa t)
        {
            try
            {
                if (t == null || t.Id != id)
                {
                    return BadRequest(error: "C�digo inv�lido");
                }

                var task = _tarefas.FirstOrDefault(x => x.Id == id);
                if(task == null)
                {
                    return NotFound(value: "C�digo n�o encontrado");
                }
                return new OkResult();
            }
            catch (Exception ex)
            {
                return BadRequest(error: ex.ToString());
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var task = _tarefas.FirstOrDefault(t => t.Id == id);
                if (task == null)
                {
                    return NotFound(value: "Tarefa n�o encontrada");
                }

                return new OkResult();
            }
            catch (Exception ex)
            {
                return BadRequest(error: ex.ToString());
            }
        }
    }
}