﻿using DDD_CRUD.Domain.Entities;

namespace DDD_CRUD.Domain.Interfaces.Repositories
{
    public interface ITarefaRepository : IRepositoryBase<Tarefa>
    {
    }
}
