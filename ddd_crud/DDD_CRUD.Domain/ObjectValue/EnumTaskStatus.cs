﻿namespace DDD_CRUD.Domain.ObjectValue
{
    public enum EnumTaskStatus
    {
        Pendente = 0,
        Concluida = 1,
        Cancelada = 2
    }
}
