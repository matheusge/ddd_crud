﻿using System;
using DDD_CRUD.Domain.ObjectValue;

namespace DDD_CRUD.Domain.Entities
{
    public class Tarefa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Concluida { get; set; }
    }
}
